<?php

return [
    'route_prefix' => '/projects',
    'middleware' => false, // If we need to secure it we use middlewares, ex: ['can:do-anything']
    'setup_routes' => false,
    'publishes_migrations' => true,
];
