<?php

namespace NaranjoArquitectura\ProjectsCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class ProjectsCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        'project' => 'ProjectCrudController'
    ];
}
