<?php

namespace NaranjoArquitectura\ProjectsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class ProjectCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'title',
                'label' => 'Nombre',
            ],
            [
                'name' => 'date',
                'label' => 'Fecha',
                'type' => 'date',
            ],
            [
                'name' => 'status',
                'label' => 'Estado',
                'type' => 'news_crud_enum',
            ],
            [
                'name' => 'featured',
                'label' => 'Destacado',
                'type' => 'check',
            ],

        ];
    }
}
