<?php

namespace NaranjoArquitectura\ProjectsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class ProjectCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'title',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.title'),
            ],
            [
                'name' => 'slug',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.slug'),
                'type' => 'text',
                'hint' => trans('NaranjoArquitectura::ProjectsCrud.slug_hint'),
            ],
            [
                'name' => 'date',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.date'),
                'type' => 'datetime_picker',
                'date_picker_options' => [
                    'todayBtn' => true,
                    'format' => 'dd-mm-yyyy',
                    'language' => 'es'
                ]
            ],
            [
                'name' => 'image',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.image'),
                'type' => 'browse'
            ],
            [
                'name' => 'content',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.content'),
                'type' => 'ckeditor'
            ],
            [
                'name' => 'status',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.status'),
                'type' => 'news_crud_enum'
            ],
            [
                'name' => 'featured',
                'label' => trans('NaranjoArquitectura::ProjectsCrud.featured'),
                'type' => 'checkbox'
            ],
        ];
    }
}
