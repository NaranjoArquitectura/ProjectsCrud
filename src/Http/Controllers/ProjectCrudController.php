<?php

namespace NaranjoArquitectura\ProjectsCrud\Http\Controllers;

use SoluAdmin\Support\Http\Controllers\BaseCrudController;
use NaranjoArquitectura\ProjectsCrud\Http\Requests\ProjectCrudRequest as StoreRequest;
use NaranjoArquitectura\ProjectsCrud\Http\Requests\ProjectCrudRequest as UpdateRequest;
use NaranjoArquitectura\ProjectsCrud\Models\Project;

class ProjectCrudController extends BaseCrudController
{

    public function setup()
    {
        parent::setup();
        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
