<?php

namespace NaranjoArquitectura\ProjectsCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProjectCrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255',
            'slug' => 'unique:' . config('NaranjoArquitectura.ProjectsCrud.tables_prefix') . 'projects,slug,' . \Request::get('id'),
            'content' => 'required|min:2',
            'status' => 'required',
            'image' => 'required',
        ];
    }
}
