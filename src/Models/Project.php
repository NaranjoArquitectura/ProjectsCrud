<?php

namespace NaranjoArquitectura\ProjectsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoluAdmin\Support\Traits\SafeSluggableScopeHelpers;
use SoluAdmin\Support\Traits\SafeSluggable;

class Project extends Model
{
    use CrudTrait;
    use HasTranslations;
    use SafeSluggable;
    use SafeSluggableScopeHelpers;


    protected $primaryKey = 'id';
    protected $fillable = ['title', 'slug', 'content', 'image', 'status', 'featured'];
    protected $translatable = ['title', 'slug','content', 'image'];
    protected $casts = [
        'featured' => 'boolean',
        'date' => 'datetime',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('SoluAdmin.ProjectsCrud.tables_prefix') . 'projects');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    public function scopePublished($query)
    {
        return $query->where('status', 'PUBLISHED')
            ->where('date', '<=', date('Y-m-d'))
            ->orderBy('date', 'DESC');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', true)
            ->where('date', '<=', Carbon::now()->toDateTimeString())
            ->orderBy('date', 'DESC');
    }

    public function getSlugOrTitleAttribute()
    {
        return ($this->slug != '') ? $this->slug : $this->title;
    }
}
