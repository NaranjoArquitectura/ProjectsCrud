<?php

return [
    'project_singular' => 'Project',
    'project_plural' => 'Projects',
    'image' => 'Imagen principal',
    'date' => 'Fecha',
    'title'  => 'Nombre del proyecto',
    'status'  => 'Estado del proyecto',
    'slug' => 'Slug (URL)',
    'slug_hint' => 'Se generará automáticamente a partir del nombre o título, si se deja vacío.',
    'content' => 'Contenido',
    'featured' => 'Destacado',
    'PUBLISHED' => 'Publicado',
    'DRAFT' => 'Borrador',
];
