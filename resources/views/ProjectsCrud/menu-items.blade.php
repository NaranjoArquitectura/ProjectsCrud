@if(\Auth::user()->hasModule('NaranjoArquitectura\\ProjectsCrud'))
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('NaranjoArquitectura.ProjectsCrud.route_prefix', '') . '/project') }}">
        <i class="fa fa-bookmark"></i> <span>{{trans('NaranjoArquitectura::ProjectsCrud.project_plural')}}</span>
    </a>
</li>
@endif